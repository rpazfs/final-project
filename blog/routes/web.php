<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about');
});

#Route::get('/signin', function () {
#    return view('signin');
#});
#Route::get('/signup', function () {
#    return view('signup');
#});
#Route::post('/home','nrController@signup');
// Route::get('/signup', function () {
//     return view('signup');
// });

// Route::get('/signin', function () {
//     return view('signin');
// });

// Route::get('/home', function () {
//     return view('innerHome');
// });

Route::get('/message', function () {
    return view('message');
});
Route::get('/profile', function () {
    return view('profile');
});

Route::get('/signin', 'nrController@index')->name('signin');
Route::post('/signin', 'nrController@signin2');
Route::get('/signup', 'nrController@create')->name('signup');
Route::post('/signup', 'nrController@store');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
     Route::get('/home', 'HomeController@index')->name('home');
     Route::get('/profile/{profileId}', 'nrController@show')->name('profile');
     Route::get('/followers', 'AuthController@followers');
     Route::get('/following', 'AuthController@following');
     #Route::get('/unfollow', 'AuthController@unfollow')->name('unfollow');
     Route::post('/profile/{profileId}', 'nrController@show')->name('profile');
     Route::post('profile/{profileId}/follow', 'nrController@followUser')->name('follow');
    Route::post('profile/{profileId}/unfollow', 'nrController@unFollowUser')->name('unfollow');
});
//Route::get('/unfollow', 'AuthController@unfollow')->name('unfollow');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/save-post', function () {
    return view('savepost');
});

Route::get('/settings', function () {
    return view('settings');
});

Route::get('/settings/change-password', function () {
    return view('changePassword');
});


Route::post('/welcome', 'AuthController@welcome');

Route::get('/post/create' , 'PostController@create');

Route::post('/post', 'PostController@store');

Route::get('/post', 'PostController@index');
Route::get('/post/{id}', 'PostController@show');
Route::get('/post/{id}/edit', 'PostController@edit');
Route::delete('/post/{id}', 'PostController@destroy');


Route::resource('/comments','CommentsController');
Route::resource('/replies','RepliesController');
Route::post('/replies/ajaxDelete','RepliesController@ajaxDelete');
Route::get('/post/{$id}', 'CommentsController@index');