<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user1=\App\User::create([
            'name'=> 'Nadia',
            'email'=> 'nadiaas@gmail.com',
            'username'=> 'nadiaas',
            'password'=> Hash::make('nadiaastrias'),
        ]);

        $user2=\App\User::create([
            'name'=> 'Astria',
            'email'=> 'astria@gmail.com',
            'username'=> 'astria',
            'password'=> Hash::make('nastrias'),
        ]);

        
        $user3=\App\User::create([
            'name'=> 'raja',
            'email'=> 'raja@gmail.com',
            'username'=> 'raja',
            'password'=> Hash::make('rajapasha'),
        ]);

        $user4=\App\User::create([
            'name'=> 'pasha',
            'email'=> 'pasha@gmail.com',
            'username'=> 'pasha',
            'password'=> Hash::make('sulaksana'),
        ]);
        App\Followship::create([
            'user_1'=> $user1->id,
            'user_2'=> 2,
        ]);
        App\Followship::create([
            'user_1'=> $user3->id,
            'user_2'=> 2,
        ]);
        App\Followship::create([
            'user_1'=> 2,
            'user_2'=> $user1->id,
        ]);
    }
}
