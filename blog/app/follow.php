<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class follow extends Model
{
    protected $table = "following";
    protected $appends = ['count'];

        public function artists()
    {
        return $this->belongsToMany(User::class,'following','user_id','active_id');
    }

    public function getFollowedCountAttribute()
    {
        return count($this->artists);
    }
}
