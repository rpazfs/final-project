<?php
function isFollow($id){
    if (\App\followship::where('user_1',$id)->where('user_2',auth()->user()->id)->exists()){
        return "follower";
    }elseif(\App\followship::where('user_2',$id)->where('user_1',auth()->user()->id)->exists()){
        return "following";
    }else{
        return "none";
    }
}