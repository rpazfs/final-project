<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\notification;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()){
            $notification=notification::where('user_id', auth()->user()->id)->get();
            $user=User::get();
            return view('innerHome',compact('notification','user'));
        }else{
            return view('signin');
        }
    }
}
