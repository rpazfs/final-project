<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;
use App\followship;
use App\notification;
use DB;
class AuthController extends Controller
{
    public function welcome(Request $request){
        return view('innerHome');
    }

    public function home(){
        if (Auth::check()){
            $followers=followship::where('user_1','!=', auth()->user()->id)->get();
            $following=followship::where('user_1', auth()->user()->id)->get();
            $notification=notification::where('user_id', auth()->user()->id)->get();
            $user=User::get();
            return view('profile',compact('followers','following','notification','user'));
        }else{
            return view('signin');
        }
    }
    
    public function followers(){
        if (Auth::check()){
            $followers=followship::where('user_1','!=', auth()->user()->id)->get();
            $following=followship::where('user_1', auth()->user()->id)->get();
            $notification=notification::where('user_id', auth()->user()->id)->get();
            $user=User::get();
            return view('followers',compact('followers','following','notification','user'));
        }else{
            return view('signin');
        }
    }
    public function following(){
        if (Auth::check()){
            $followers=followship::where('user_1','!=', auth()->user()->id)->get();
            $following=followship::where('user_1', auth()->user()->id)->get();
            $notification=notification::where('user_id', auth()->user()->id)->get();
            $user=User::get();
            return view('following',compact('followers','following','notification','user'));
        }else{
            return view('signin');
        }
    }
    public function unfollow(Request $request){
        if (Auth::check()){
            if($request->action=='unfollow'){
            //if(followship::where('user_1',auth()->user()->id)->where('user_2',auth()->user()->id)->exists()){
            if(followship::where('user_1','!=', auth()->user()->id)->where('user_2', auth()->user()->id)->exists());{
                return view('signin');
                // $data=followship::where('user_1','!=', auth()->user()->id)->where('user_2', auth()->user()->id)->first();
                // $data->delete();
            }
        }
        }
        return dd($request->all());
    }
    public function follow (User $user)
    {
        $follower = Auth :: user ();
        // Are you following
        $is_following = $followers->isFollowing ($user->id);
        if (! $Is_following) {
            // Follow if not following
            $followers->follow ($user->id);
            return back ();
        }
    }

}