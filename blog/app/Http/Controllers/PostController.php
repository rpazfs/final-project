<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\notification;
use App\User;
use DB;
use App\Comment;

class PostController extends Controller
{
    public function create(){
        return view('post.create');
    }

    public function store(Request $request) {

        $query = DB::table('posts')-> insert([
            "content" => $request["content"],
            "image" => $request["image"],
            'user_id' => auth()->id()
        ]);

        return redirect('/post');
    }

    public function index(Request $request) {
        $posts = DB::table('posts')->get();
        return view('post.index', compact('posts'));
    }

    public function show($id) {
        $posts = DB::table('posts')->where('id', $id)->first();
        return view('post.show', compact('posts'));
        $comments = Comment::latest('created_at')->get();
        return view('post.show', ['comments' => $comments]);
    }

    public function edit($id) {
        $posts = DB::table('posts')->where('id', $id)->first();
        return view('post.show', compact('posts'));
    }

    public function destroy($id) {
        $posts = DB::table('posts')->where('id', $id)->delete();
        return redirect('/post')->with('success','Post berhasil dihapus!');
    }

    
}
