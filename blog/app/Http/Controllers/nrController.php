<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Input;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Session;
//use Illuminate\Support\Facades\Request;
use DB;
//use App\User;
use Hash;

class nrController extends Controller
{
 
    // public function signup(Request $request)
    // {
    //     $request->validate([
    //         'name' => 'required',
    //         'email' => 'required',
    //         'password' => 'required',
    //         'username' => 'required|unique:users',
    //     ]);
    //     // $query = DB::table('users')->insert([
    //     //     "name" => $request["name"],
    //     //     "email" => $request["email"],
    //     //     "password" => $request["password"],
    //     //     "username" => $request["username"]
    //     // ]);
    //     // return redirect('/signup');
    // }

    public function create()
    {
        return view('signup');
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'username' => 'required',
        ]);
        

        $user = User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);
        return redirect()->route('signin');
    }

    public function index()
    {
        return view('signin');
    }


    public function signin2(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password'=> 'required|string'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput($request->all);
        }else{
        $data = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];
            Auth::attempt($data);
            if (Auth::user()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
                return redirect()->route('home');
            } else { // false
            //Login Fail
            Session::flash('error', 'Email atau password salah');
                return redirect()->route('signin');
            }
        }
    }

    public function post(){
        $request->validate([
            'content' => 'required',
            'foto' => 'required',
        ]);
        $post= new Post;
        $post->content=$request["content"];
        $post->foto=$request["foto"];
        $post->save();
        return redirect('/post');
    }

    public function followUser(int $profileId)
    {
      $user = User::find($profileId);
      if(! $user) {
        
         return redirect()->back()->with('error', 'User does not exist.'); 
     }
    
        $user->followings()->attach(auth()->user()->id);
       
        return redirect()->back()->with('success', 'Successfully followed the user.');
    }

    public function unfollowUser(int $profileId)
    {
      $user = User::find($profileId);
      if(! $user) {
        
         return redirect()->back()->with('error', 'User does not exist.'); 
     }
    
        $user->followings()->detach(auth()->user()->id);        
        return redirect()->back()->with('success', 'Successfully followed the user.');
    }
    
    public function home(){
        if (Auth::check()){
            return view('profile');
        }else{
            return view('signin');
        }
    }

    public function show(int $profileId){
        $user = User::find($profileId);
        $followers = $user->followers()->get();
        $followings = $user->followings;
        $isi=User::get();
        return view('profile',['user'=>$user, 'followers'=>$followers, 'followings'=>$followings,'isi'=>$isi]);
    }

    public function getArtists(Request $request)
    {
        $user_id = $request->get('user_id');
        // get the user using the user_id
        $follower = User::with('artists')->find($user_id);
        return response()->json(['data' => $follower ], 200);
     }
}

