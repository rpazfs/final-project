<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('/Css/style.css')}}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@500;800&family=Nunito:wght@400;600;800&display=swap" rel="stylesheet">
    <title>Naragram - Sign In</title>
</head>

<body>
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand font-weight-bold" href="/" style="color:#503AD8; font-family: 'Heebo', sans-serif;">Naragram</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mx-auto mt-2 mt-lg-0" style="font-family: 'heebo', sans-serif;">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about">About </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contact">Contact</a>
                    </li>
                </ul>
                <a class="btn btn-secondary ml-4" href="{{ route('signin') }}" role="button">Sign In</a>
                <a class="btn btn-primary ml-2" href="{{ route('signup') }}" role="button">Sign Up</a>
            </div>
        </nav>
        <header class="mt-5">
            <div class="row">
                <div class="col align-self-center">
                    <h1 class="font-weight-bold" style="font-family: 'heebo', sans-serif;">Sign In to Naragram <br>Connect with Other People</h1>
                    <br>
                    <p style=" font-family: 'Nunito' , sans-serif ;">
                        If you don’t have an account <br> You can <a style="color: #503AD8; font-weight: 900;" href="/signup">Register Here!</a>
                    </p>
                    <img style="margin-left: 150px;" src="{{asset('/images/sign.png')}}" alt="">
                </div>
                <div class="col">
                    <form action="{{ route('signin') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group ">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter your password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group mb-5">
                            <div class="form-check form-inline">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" style="font-size:12px;" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                            </div>
                        </div>
                                
                        <button type="submit" class="btn btn-primary mb-4" style="width: 100%;">Sign In</button>
                        <p class="text-center">or continue with</p>
                        <div class="row text-center mt-4">
                            <div class="col">
                                <a href="#"><img src="/images/buttonfacebook.png" alt=""></a>
                            </div>
                            <div class="tombol-2 col">
                                <a href="#"><img src="/images/buttongoogle.png" alt=""></a>
                            </div>
                            <div class="tombol-3 col">
                                <a href="#"><img src="/images/buttontwitter.png" alt=""></a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </header>
    </div>

    <footer class="mt-5">
        <div class="naragram mt-5 text-center">
            <p class="font-weight-bold" style="color:#503AD8;font-family: 'heebo', sans-serif;font-size: 24px; ">Naragram</p>
            <div class="contact mt-4 pb-4">
                <a href="#" class="mr-5"><img src="{{asset('/images/facebook.png')}}" alt=""></a>
                <a href="#" class="mr-5"><img src="{{asset('/images/email.png')}}" alt=""></a>
                <a href="#" class="mr-5"><img src="{{asset('/images/wa.png')}}" alt=""></a>
                <a href="#"><img src="{{asset('/images/twitter.png')}}" alt=""></a>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>