
            
            <a class=" mr-3" href="#"><i style="color: #565656;" class="fas fa-bell"></i>{{$notification->count()}}</a>
            <a class=" mr-3" href="#"><i class="fas fa-heart" style="color: #565656;"></i></a>
            <li class="nav-item dropdown" style="list-style:none;">
                <a id="navbarDropdown" style="color: #25396F;" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <img class="mr-3" src="/images/raja.png" height="37px" alt="">
                                
                    @rpazfs<span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
