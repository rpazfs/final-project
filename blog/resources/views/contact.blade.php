<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('/Css/style.css')}}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@500;800&family=Nunito:wght@400;600;800&display=swap" rel="stylesheet">
    <title>Naragram - Contact</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand font-weight-bold" href="/" style="color:#503AD8; font-family: 'Heebo', sans-serif;">Naragram</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mx-auto mt-2 mt-lg-0" style="font-family: 'heebo', sans-serif;">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about">About </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/contact">Contact</a>
                    </li>
                </ul>
                <a class="btn btn-secondary ml-4" href="/signin" role="button">Sign In</a>
                <a class="btn btn-primary ml-2" href="/signup" role="button">Sign Up</a>
            </div>
        </nav>
        <header class="mt-5">
            <h1 class="text-center font-weight-bold mb-5" style="font-family: 'heebo', sans-serif;">Contact Information</h1>
            <div class="row">
                <div class="col align-self-center">
                    <p style=" font-family: 'Nunito' , sans-serif ;">
                        <img class="mr-5" src="/images/twitter.png" alt=""> @Naragram
                    </p>
                    <p style=" font-family: 'Nunito' , sans-serif ;">
                        <img class="mr-5" src="/images/phone.png" alt=""> +022 1112 0805
                    </p>
                    <p style=" font-family: 'Nunito' , sans-serif ;">
                        <img class="mr-5" src="/images/address.png" alt=""> Jln. Naragram, No 125, Bandung, Jawa Barat, Indonesia.
                    </p>
                    <p style=" font-family: 'Nunito' , sans-serif ;">
                        <img class="mr-5" src="/images/email.png" alt=""> naragram@gmail.com
                    </p>
                </div>
                <div class="ml-auto">
                    <img src="/images/contact.png" alt="">
                </div>
            </div>
        </header>
    </div>

    <footer class="mt-5">
        <div class="action container form-inline">
            <h4 class="ml-3">Interested surfing with Naragram ?</h4>
            <a class="btn btn-primary ms-2 ml-auto mr-3" href="/Html/SignIn.html" role="button">Get Started</a>
        </div>
        <div class="naragram mt-5 text-center">
            <p class="font-weight-bold" style="color:#503AD8;font-family: 'heebo', sans-serif;font-size: 24px; ">Naragram</p>
            <div class="contact mt-4 pb-4">
                <a href="#" class="mr-5"><img src="{{asset('/images/facebook.png')}}" alt=""></a>
                <a href="#" class="mr-5"><img src="{{asset('/images/email.png')}}" alt=""></a>
                <a href="#" class="mr-5"><img src="{{asset('/images/wa.png')}}" alt=""></a>
                <a href="#"><img src="{{asset('/images/twitter.png')}}" alt=""></a>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>