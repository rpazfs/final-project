<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Naragram - Home</title>

    <!-- bootstrap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- font heebo sama nunito -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- style css eksternal -->
    <link rel="stylesheet" href="{{asset('/Css/style.css')}}">

    <!-- icon font awesome -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@500;800&family=Nunito:wght@400;600;800&display=swap" rel="stylesheet">
</head>

<body>

    <!-- awal code -->
    <div class="row">
        <!-- sidebar -->
        <div class="col  my-2 mx-2">

            <!-- logo sama search -->
            <header class=" form-inline mb-4">
                <a href="#" class="logo"><img src="{{asset('/images/logo.png')}}" alt=""></a>
                <input class="form-control ml-sm-2" style="width: 85%;" type="search" placeholder="Search" aria-label="Search">
            </header>
            <!-- logo sama search samoe sini -->


            <div class="sidebar">

                <!-- menu -->
                <div class="menu">
                    <h5>Menu</h5>
                    <hr>
                </div>
                <div class=" mb-4">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                            <ul class="navbar-nav mt-2 mt-lg-0 d-block text-left" style="font-family: 'heebo', sans-serif;">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/home" style="color: #503AD8;"><i class='fas fa-home mr-5'></i>Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/message"><i class='fas fa-comment-dots mr-5'></i>Message</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/profile"><i class='fas fa-user-alt mr-5'></i>Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/save-post"><i class='fas fa-bookmark mr-5'></i> Save Post</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/settings"><i class='	fas fa-cog mr-5'></i>Settings</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!-- menu sampe sini -->

                <!-- friend -->
                <div class="heading">
                    <h5>Friends</h5>
                    <hr>
                </div>
                <div class="friends">
                    <div class="first">
                        <a class="form-inline" style="align-items: unset;" href="#">
                            <img src="{{asset('/images/raja.png')}}" height="37px" alt="">
                            <div class="nama ml-5">
                                <h5>Raja Pasha A.Z.F.S.</h5>
                                <p>@rpazfs</p>
                            </div>
                            <p>online</p>
                        </a>
                    </div>
                    <div class="second">
                        <a class="form-inline" style="align-items: unset;" href="#">
                            <img src="{{asset('/images/Nadia.png')}}" height="37px" alt="">
                            <div class="nama ml-5">
                                <h5>Nadia Astria Savitri</h5>
                                <p>@nadiaas</p>
                            </div>
                            <p>online</p>
                        </a>
                    </div>
                    <div class="third">
                        <a class="form-inline" style="align-items: unset;" href="#">
                            <img src="{{asset('/images/raja.png')}}" height="37px" alt="">
                            <div class="nama ml-5">
                                <h5>Raja Pasha A.Z.F.S.</h5>
                                <p>@rpazfs</p>
                            </div>
                            <p>online</p>
                        </a>
                    </div>
                    <div class="forth">
                        <a class="form-inline" style="align-items: unset;" href="#">
                            <img src="{{asset('/images/Nadia.png')}}" height="37px" alt="">
                            <div class="nama ml-5">
                                <h5>Nadia Astria Savitri</h5>
                                <p>@nadiaas</p>
                            </div>
                            <p>online</p>
                        </a>
                    </div>
                    <div class="fifth">
                        <a class="form-inline" style="align-items: unset;" href="#">
                            <img src="{{asset('/images/raja.png')}}" height="37px" alt="">
                            <div class="nama ml-5">
                                <h5>Raja Pasha A.Z.F.S.</h5>
                                <p>@rpazfs</p>
                            </div>
                            <p>online</p>
                        </a>
                    </div>
                </div>
                <!-- friend sampe sini -->

            </div>
        </div>
        <!-- sidebar sampe sini -->

        <!-- main content -->
        <div class="col-6" style="background-color: #F9F9F9;">

            <!-- story -->
            <div class="story form-inline justify-content-center mt-2">
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>you</p>
                </a>
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
                <a class="mr-4" href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
                <a href="#">
                    <img src="{{asset('/images/raja.png')}}" height="54px" alt="">
                    <p>@rpazfs</p>
                </a>
            </div>
            <hr class="mt-0">
            <!-- story sampe sini -->

            <!-- feed -->
            <div class="feeds">

                <!-- heading feed -->
                <div class="heading">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <h5 style="color: #25396F;">Feeds</h5>
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0" style="font-family: 'heebo', sans-serif;">
                            <li class="nav-item active">
                                <a class="nav-link" href="/Html/home.html">All</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Html/about.html">Following</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Html/contact.html">Popular</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- heading feed sampe sini -->

                <!-- kolom input post -->
                
                <!-- kolom input post sampe sini -->

                <!-- post di feeds -->
                <div class="post form-inline justify-content-center">

                    <!-- ini buat satu post -->
                    <div class="posted mr-4">
                        <div class="header form-inline  align-self-center">
                            <a class="form-inline ml-3 mt-3 " style="align-items: center;" href="#">
                                <img src="{{asset('/images/raja.png')}}" height="30px" alt="">
                                <h5 class="ml-2" style="color: #503AD8; font-size: 14px; font-weight: 700;">Raja Pasha A.Z.F.S.</h5>
                            </a>
                            <a class="ml-auto mr-3 mt-2" style="color: #565656;" href="#"><i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="content mt-2">
                            <img src="" width="330px" alt="">
                        </div>
                        <div class="likes form-inline">
                            <a class="align-self-center ml-2" href="#"><i class="fas fa-heart" style="color: red;"><span class="ml-1" style="font-size: 12px; font-family: 'Nunito', sans-serif; color: #565656;">1205 likes</span></i></a>
                            <a style="color: #565656;" class="ml-3" href="#"><i class='fas fa-comment-dots'></i></a>
                            <a class="ml-auto" style="color: #565656;" href="#"><i class='fas fa-bookmark mr-2'></i></a>
                        </div>
                        <div class="comment mt-2  ml-2 ">
                            <div class="user form-inline">
                                <h5 class="my-0 mr-2" style="font-size: 14px;">@nadiaas</h5>
                                <p class="my-0" style="font-size: 12px; line-height: 100%;">Learning is the best thing to do when ..</p>
                            </div>
                            <a style="color: #565656; font-size: 12px;" href="#">view all 500 comments</a>
                        </div>
                    </div>
                    <!-- satu post sampe sini -->

                    <!-- ini buat satu post -->
                    <div class="posted">
                        <div class="header form-inline  align-self-center">
                            <a class="form-inline ml-3 mt-3 " style="align-items: center;" href="#">
                                <img src="{{asset('/images')}}" height="30px" alt="">
                                <h5 class="ml-2" style="color: #503AD8; font-size: 14px; font-weight: 700;">Raja Pasha A.Z.F.S.</h5>
                            </a>
                            <a class="ml-auto mr-3 mt-2" style="color: #565656;" href="#"><i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="content mt-2">
                            <img src="" width="330px" alt="">
                        </div>
                        <div class="likes form-inline">
                            <a class="align-self-center ml-2" href="#"><i class="fas fa-heart" style="color: red;"><span class="ml-1" style="font-size: 12px; font-family: 'Nunito', sans-serif; color: #565656;">1205 likes</span></i></a>
                            <a style="color: #565656;" class="ml-3" href="#"><i class='fas fa-comment-dots'></i></a>
                            <a class="ml-auto" style="color: #565656;" href="#"><i class='fas fa-bookmark mr-2'></i></a>
                        </div>
                        <div class="comment mt-2  ml-2 ">
                            <div class="user form-inline">
                                <h5 class="my-0 mr-2" style="font-size: 14px;">@nadiaas</h5>
                                <p class="my-0" style="font-size: 12px; line-height: 100%;">Learning is the best thing to do when ..</p>
                            </div>
                            <a style="color: #565656; font-size: 12px;" href="#">view all 500 comments</a>
                        </div>
                    </div>
                    <!-- satu post sampe sini -->

                    <!-- ini buat satu post -->
                    <div class="posted mr-4">
                        <div class="header form-inline  align-self-center">
                            <a class="form-inline ml-3 mt-3 " style="align-items: center;" href="#">
                                <img src="{{asset('/images/raja.png')}}" height="30px" alt="">
                                <h5 class="ml-2" style="color: #503AD8; font-size: 14px; font-weight: 700;">Raja Pasha A.Z.F.S.</h5>
                            </a>
                            <a class="ml-auto mr-3 mt-2" style="color: #565656;" href="#"><i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="content mt-2">
                            <img src="" width="330px" alt="">
                        </div>
                        <div class="likes form-inline">
                            <a class="align-self-center ml-2" href="#"><i class="fas fa-heart" style="color: red;"><span class="ml-1" style="font-size: 12px; font-family: 'Nunito', sans-serif; color: #565656;">1205 likes</span></i></a>
                            <a style="color: #565656;" class="ml-3" href="#"><i class='fas fa-comment-dots'></i></a>
                            <a class="ml-auto" style="color: #565656;" href="#"><i class='fas fa-bookmark mr-2'></i></a>
                        </div>
                        <div class="comment mt-2  ml-2 ">
                            <div class="user form-inline">
                                <h5 class="my-0 mr-2" style="font-size: 14px;">@nadiaas</h5>
                                <p class="my-0" style="font-size: 12px; line-height: 100%;">Learning is the best thing to do when ..</p>
                            </div>
                            <a style="color: #565656; font-size: 12px;" href="#">view all 500 comments</a>
                        </div>
                    </div>
                    <!-- satu post sampe sini -->

                    <!-- ini buat satu post -->
                    <div class="posted">
                        <div class="header form-inline  align-self-center">
                            <a class="form-inline ml-3 mt-3 " style="align-items: center;" href="#">
                                <img src="{{asset('/images/raja.png')}}" height="30px" alt="">
                                <h5 class="ml-2" style="color: #503AD8; font-size: 14px; font-weight: 700;">Raja Pasha A.Z.F.S.</h5>
                            </a>
                            <a class="ml-auto mr-3 mt-2" style="color: #565656;" href="#"><i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="content mt-2">
                            <img src="" width="330px" alt="">
                        </div>
                        <div class="likes form-inline">
                            <a class="align-self-center ml-2" href="#"><i class="fas fa-heart" style="color: red;"><span class="ml-1" style="font-size: 12px; font-family: 'Nunito', sans-serif; color: #565656;">1205 likes</span></i></a>
                            <a style="color: #565656;" class="ml-3" href="#"><i class='fas fa-comment-dots'></i></a>
                            <a class="ml-auto" style="color: #565656;" href="#"><i class='fas fa-bookmark mr-2'></i></a>
                        </div>
                        <div class="comment mt-2  ml-2 ">
                            <div class="user form-inline">
                                <h5 class="my-0 mr-2" style="font-size: 14px;">@nadiaas</h5>
                                <p class="my-0" style="font-size: 12px; line-height: 100%;">Learning is the best thing to do when ..</p>
                            </div>
                            <a style="color: #565656; font-size: 12px;" href="#">view all 500 comments</a>
                        </div>
                    </div>
                    <!-- satu post sampe sini -->

                </div>
                <!-- post di feed sampe sini -->

            </div>
            <!-- feed sampe sini -->

        </div>
        <!-- main content sampe sini -->

        <!-- sidebar kanan -->
        <div class="col">

            <!-- logo user sama notif -->
            <div class="header form-inline my-2 ml-2 mr-auto d-flex justify-content-end " style="align-items:center;">
                <a class="btn btn-primary mr-2 pt-1 pb-1" href="/post/create" role="button" type="submit">Create Post</a>
                <a  href="#"><i style="color: #565656;" class="fas fa-bell"></i>{{$notification->count()}}</a>
                <li class="nav-item dropdown" style="list-style:none;">
                    <a id="navbarDropdown" style="color: #25396F;" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <img class="mr-3" src="/images/raja.png" height="37px" alt="">
                                    
                        @<span>{{auth()->user()->username}}</span><span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </div>
            <!-- logo user sama notif sampe sini -->

            <!-- suggestion-->
            <div class="suggestion mt-4">

                <!-- header suggestion -->
                <div class="header form-inline">
                    <h5 class="mb-0" style="color: #25396F;">Suggestions for you</h5>
                    <a style="color: #503AD8; font-weight: bold;" class="ml-auto" href="#">See all</a>
                </div>
                <!-- header suggestion sampe sini -->

                <!-- friend suggestion -->
                <div class="friends">

                    <!-- satu friend suggestion -->
                    <div class="first my-4 d-block">
                        <a class="form-inline" style="align-items: center;" href="#">
                            <img src="/images/Nadia.png" height="37px" alt="">
                            <div class="nama ml-4 mr-5">
                                <h5>Nadia Astria Savitri</h5>
                                <p class="my-0" style="line-height: 150%;">@nadiaas</p>
                            </div>
                            <button class="btn btn-primary ml-auto" style="font-size: 14px; padding:5px 15px 5px 15px;">Follow</button>
                        </a>
                    </div>
                    <!-- satu friend suggestion sampe sini -->

                    <!-- satu friend suggestion -->
                    <div class="first my-4 d-block">
                        <a class="form-inline" style="align-items: center;" href="#">
                            <img src="/images/raja.png" height="37px" alt="">
                            <div class="nama ml-4 mr-5">
                                <h5>Raja Pasha A.Z.F.S.</h5>
                                <p class="my-0" style="line-height: 150%;">@rpazfs</p>
                            </div>
                            <button class="btn btn-primary ml-auto" style="font-size: 14px; padding:5px 15px 5px 15px;">Follow</button>
                        </a>
                    </div>
                    <!-- satu friend suggestion sampe sini -->

                    <!-- satu friend suggestion -->
                    <div class="first my-4 d-block">
                        <a class="form-inline" style="align-items: center;" href="#">
                            <img src="/images/Nadia.png" height="37px" alt="">
                            <div class="nama ml-4 mr-5">
                                <h5>Nadia Astria Savitri</h5>
                                <p class="my-0" style="line-height: 150%;">@nadiaas</p>
                            </div>
                            <button class="btn btn-primary ml-auto" style="font-size: 14px; padding:5px 15px 5px 15px;">Follow</button>
                        </a>
                    </div>
                    <!-- satu friend suggestion sampe sini -->
                    <hr>
                </div>
                <!-- friend suggestion sampe sini -->

            </div>
            <!-- suggestion sampe sini -->

            <!-- recently viewed -->
            <div class="recently">

                <!-- heading recently viewed -->
                <div class="header form-inline">
                    <h5 class="mb-0" style="color: #25396F;">Recently Viewed</h5>
                    <a style="color: #503AD8; font-weight: bold;" class="ml-auto" href="#">See all</a>
                </div>
                <!-- heading recently viewed sampe sini -->

                <!-- satu post -->
                <div class="posted">
                    <div class="header form-inline  align-self-center">
                        <a class="form-inline ml-3 mt-3 " style="align-items: center;" href="#">
                            <img src="/images/raja.png" height="30px" alt="">
                            <h5 class="ml-2" style="color: #503AD8; font-size: 14px; font-weight: 700;">Raja Pasha A.Z.F.S.</h5>
                        </a>
                        <a class="ml-auto mr-3 mt-2" style="color: #565656;" href="#"><i class="fas fa-ellipsis-h"></i></a>
                    </div>
                    <div class="content mt-2">
                        <img src="/images/post.png" width="330px" alt="">
                    </div>
                    <div class="likes form-inline">
                        <a class="align-self-center ml-2" href="#"><i class="fas fa-heart" style="color: red;"><span class="ml-1" style="font-size: 12px; font-family: 'Nunito', sans-serif; color: #565656;">1205 likes</span></i></a>
                        <a style="color: #565656;" class="ml-3" href="#"><i class='fas fa-comment-dots'></i></a>
                        <a class="ml-auto" style="color: #565656;" href="#"><i class='fas fa-bookmark mr-2'></i></a>
                    </div>
                    <div class="comment mt-2  ml-2 ">
                        <div class="user form-inline">
                            <h5 class="my-0 mr-2" style="font-size: 14px;">@nadiaas</h5>
                            <p class="my-0" style="font-size: 12px; line-height: 100%;">Learning is the best thing to do when ..</p>
                        </div>
                        <a style="color: #565656; font-size: 12px;" href="#">view all 500 comments</a>
                    </div>
                </div>
                <!-- satu post sampe sini -->

            </div>
            <!-- recently viewed sampe sini -->

        </div>
        <!-- sidebar kanan sampe sini -->

    </div>
    <!-- akhir code -->

    <!-- script js bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>